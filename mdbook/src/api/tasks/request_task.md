# Request Task

This Endpoint is used for transitioning tasks between statuses, such as `PUBLISHED` and `CLAIMED`.
In other words, this endpoint is used for claiming missions.

## URI

> **POST**
> ```
> /api/tasks/v1/organizations/{orgSlug}/listings/{listingSlug}/campaigns/{campaignUUID}/tasks/{taskUUID}/transitions
> ```

## Parameters

### URL

> `orgSlug`
> * Description: The 10 character slug of the organization
> * Type: `string`
> * Accepted Values: Organization Slug
> * Optional: `no`

> `listingSlug`
> * Description: The 10 character slug of the listing
> * Type: `string`
> * Accepted Values: Listing Slug
> * Optional: `no`

> `campaignUUID`
> * Description: The UUID of the task campaign
> * Type: `uuid`
> * Accepted Values: UUID
> * Optional: `no`

> `taskUUID`
> * Description: The UUID of the task
> * Type: `uuid`
> * Accepted Values: UUID
> * Optional: `no`

### POST Body

> Claim a Task
> ```json
> {"type":"CLAIM"}
> ```

> Release a Task
> ```json
> {"type":"DISCLAIM"}
> ```

> Submit a Task
> ```json
> {"type":"COMPLETE"}
> ```

## Examples

> **Claim a Task**
> ```http
> POST /api/tasks/v1/organizations/AAAAAAAAAA/listings/0000000000/campaigns/zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz/tasks/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/transitions HTTP/1.1
> Host: platform.synack.com
> User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
> Accept: application/json
> Accept-Language: en-US,en;q=0.5
> Accept-Encoding: gzip, deflate
> Referer: https://platform.synack.com/tasks/user/available
> X-CSRF-Token: xxxx
> Authorization: Bearer ${accessToken}
> Content-Type: application/json
> Origin: https://platform.synack.com
> Content-Length: 16
> Connection: close
> Cookie: visid_incap_x0x0=x0x0x0x0; visid_incap_x0x0=x0x0x0x0; fs_uid=x0x0x0x0; visid_incap_x0x0=x0x0x0; _ga=x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0
>
> {"type":"CLAIM"}
> ```
>
> **Success Response**
> ```http
> HTTP/1.1 201 Created
> Server: nginx/1.17.3
> Date: 01 Jan 2021 00:00:00 GMT
> Content-Type: application/json; charset=UTF-8
> Cache-Control: no-cache, no-store no-transform, max-age=0, must-revalidate
> Location: /organizations/AAAAAAAAAA/listings/0000000000/campaigns/zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz/tasks/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/transitions/x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0
> X-Correlation-Id: x0x0x0x0
> Via: 1.1 google
> Alt-Svc: clear
> Connection: close
> X-CDN: Incapsula
> X-Iinfo: x-x0x0x0x0-x0x0x0x0 NNYY CT(0 0 0) RT(x0x0x0x0x0x0x x) q(0 0 0 -1) r(2 2) U6
> Content-Length: 246
>
> {
>   "id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>   "objectId":"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
>   "objectType":"task",
>   "type":"CLAIM",
>   "comment":"",
>   "createdOn":"2021-01-01T00:00:00.000000000Z",
>   "createdBy":"srtmember1",
>   "claims":{"current":1,"limit":5}
> }
> ```
>
> **Forbidden Response**  
> (Task is not able to be claimed because it is not being publicly available (someone else already claimed it, etc.))  
> This *MIGHT* be the response for a task you try to claim during downtime, but I need to double check.
> ```
> HTTP/1.1 403 Forbidden
>
> You are not authorized to access this resource to perform this action.
> ```
>
> Max Tasks Already Claimed
> ```
> HTTP/1.1 412 Precondition Failed
>
> {
>   "detail": "The transition CLAIM cannot be performed on this resource at this moment.\nThe maximum number of concurrent CLAIMED/IN REVIEW tasks has been reached for user srtmember1",
>   "errorType": "Transition",
>   "status": 412,
>   "title": "maxClaimedTasksReached
> }
> ```

> **Release a Task**
> ```http
> POST /api/tasks/v1/organizations/AAAAAAAAAA/listings/0000000000/campaigns/zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz/tasks/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/transitions HTTP/1.1
> Host: platform.synack.com
> User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
> Accept: application/json
> Accept-Language: en-US,en;q=0.5
> Accept-Encoding: gzip, deflate
> Referer: https://platform.synack.com/tasks/user/claimed
> X-CSRF-Token: xxxx
> Authorization: Bearer ${accessToken}
> Content-Type: application/json
> Origin: https://platform.synack.com
> Content-Length: 16
> Connection: close
> Cookie: visid_incap_x0x0=x0x0x0x0; visid_incap_x0x0=x0x0x0x0; fs_uid=x0x0x0x0; visid_incap_x0x0=x0x0x0; _ga=x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0
>
> {"type":"DISCLAIM"}
> ```
> 
> **Success Response**
> ```http
> HTTP/1.1 201 Created
> Server: nginx/1.17.3
> Date: 01 Jan 2021 00:00:00 GMT
> Content-Type: application/json; charset=UTF-8
> Cache-Control: no-cache, no-store no-transform, max-age=0, must-revalidate
> Location: /organizations/AAAAAAAAAA/listings/0000000000/campaigns/zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz/tasks/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/transitions/x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0
> X-Correlation-Id: x0x0x0x0
> Via: 1.1 google
> Alt-Svc: clear
> Connection: close
> X-CDN: Incapsula
> X-Iinfo: x-x0x0x0x0-x0x0x0x0 NNYY CT(0 0 0) RT(x0x0x0x0x0x0x x) q(0 0 0 -1) r(2 2) U6
> Content-Length: 246
>
> {
>   "id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>   "objectId":"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
>   "objectType":"task",
>   "type":"DISCLAIM",
>   "comment":"",
>   "createdOn":"2021-01-01T00:00:00.000000000Z",
>   "createdBy":"srtmember1",
>   "claims":{"current":0,"limit":5}
> }
> ```

