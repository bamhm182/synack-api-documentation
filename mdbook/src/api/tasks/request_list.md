# Request List of Tasks

This Endpoint is used for requesting a list of missions when you visit `https://platform.synack.com/tasks/user/{available,claimed,review,completed}`

## URI

> **GET**
> ```
> /api/tasks/v1/tasks{?assetTypes,listingUid,page,pageSize,status,taskType,withHasBeenViewedInfo}
> ```

## Parameters

### GET

> `assetTypes`
> * Description: Category to filter on (Android, iOS, Host, Web, etc.)
> * Type: `string`
> * Accepted Values: `web`, `host`, `android`, `ios` (Note that multiple categories are separate parameters, such as `assetTypes=ios&assetTypes=android`)
> * Optional: `yes`

> `listingUid`
> * Description: Slug of a specific Listing to check for missions
> * Type: `string`
> * Accepted Values: Any Listing Slug
> * Optional: `yes`

> `page`
> * Description: Page number
> * Type: `number`
> * Accepted Values: Any integer
> * Optional: `yes`

> `pageSize`
> * Description: Number of tasks on a given page
> * Type: `number`
> * Accepted Values: Any integer
> * Optional: `yes`

> `status`
> * Description: Grouping of missions (tabs)
> * Type: `string`
> * Accepted Values: `PUBLISHED`, `CLAIMED`, `FOR_REVIEW`, `APPROVED`
> * Optional: `yes`

> `taskType`
> * Description: Type of task (Missions, Suspected Vuln to Mission, etc.)
> * Type: `string`
> * Accepted Values: `mission`, `sv2m` (Note that multiple types are separate parameters, such as `taskType=missions&taskType=sv2m`)
> * Optional: `yes`

> `withHasBeenViewedInfo`
> * Description: Should it include information on whether you have seen the mission already?
> * Type: `boolean`
> * Accepted Values: `true`, `false`
> * Optional: `yes`

## Examples

> Request List of Completed Tasks
> ```http
> GET /api/tasks/v1/tasks?withHasBeenViewedInfo=true&status=ACCEPTED&page=0&pageSize=20 HTTP/1.1
> Host: platform.synack.com
> User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
> Accept: application/json
> Accept-Language: en-US,en;q=0.5
> Accept-Encoding: gzip, deflate
> Referer: https://platform.synack.com/tasks/user/completed
> X-CSRF-Token: xxxx
> Authorization: Bearer ${accessToken}
> Content-Type: application/json
> Connection: close
> Cookie: visid_incap_x0x0=x0x0x0x0; visid_incap_x0x0=x0x0x0x0; fs_uid=x0x0x0x0; visid_incap_x0x0=x0x0x0; _ga=x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0; incap_ses_x0_x0x0=x0x0x0
> ```
>
> No Missions
> ```http
> HTTP/1.1 200 OK
> Server: nginx/1.17.3
> Date: 01 Jan 2021 00:00:00 GMT
> Content-Type: application/json; charset=UTF-8
> Cache-Control: no-cache, no-store no-transform, max-age=0, must-revalidate
> Vary: Accept-Encoding
> X-Correlation-Id: x0x0x0x0
> X-Pagination: {"total_pages":0,"current_page":0,"total_entries":0}
> Via: 1.1 google
> Alt-Svc: clear
> Connection: close
> X-CDN: Incapsula
> X-Iinfo: x-x0x0x0x0-x0x0x0x0 NNNY CT(0 0 0) RT(x0x0x0x0x0x0x x) q(0 0 0 -1) r(46 46) U16
> Content-Length: 3
>
> []
> ```
>
> One Mission (Approved)
> ```http
> HTTP/1.1 200 OK
> Server: nginx/1.17.3
> Date: 01 Jan 2021 00:00:00 GMT
> Content-Type: application/json; charset=UTF-8
> Cache-Control: no-cache, no-store no-transform, max-age=0, must-revalidate
> Vary: Accept-Encoding
> X-Correlation-Id: x0x0x0x0
> X-Pagination: {"total_pages":0,"current_page":0,"total_entries":1}
> Via: 1.1 google
> Alt-Svc: clear
> Connection: close
> X-CDN: Incapsula
> X-Iinfo: x-x0x0x0x0-x0x0x0x0 NNNY CT(0 0 0) RT(x0x0x0x0x0x0x x) q(0 0 0 -1) r(46 46) U16
> Content-Length: x0x0
>
> [
>   {
>     "publishedOn":"2021-01-01T00:00:00.000Z",
>     "response":"Text that is currently within the textarea on the mission page",
>     "deactivatedOn":null,
>     "task_group":null,
>     "modifiedBy":"",
>     "taskType":"MISSION",
>     "sv":[""],
>     "taskTemplate":{
>       "id":"x0x0x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>       "version":1
>      },
>      "claimedOn":"2021-01-01T00:00:00.000Z",
>      "submissionsCount":1,
>      "campaign":{
>        "id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>        "title":"SLEEPYPUPPY JAN2021"
>      },
>      "assignee":"srtmember1",
>      "createdBy":"",
>      "attackType":["AttackType"],
>      "title":"Mission Title",
>      "listing":{
>        "id":"0000000000",
>        "title":"sleepypuppy"
>      },
>      "definition":{
>        "id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>        "name":"PCI DSS 11.3"
>      },
>      "validResponses":[
>        {"label":"Yes","value":"yes"},
>        {"label":"No","value":"no"},
>        {"label":"N/A","value":"n/a"}
>      ],
>      "invalidatedOn":null,
>      "":null,
>      "payout":{
>        "amount":25,
>        "currency":"USD"
>      },
>      "cwe":[""],
>      "isAssigneeCurrentUser":true,
>      "pausedOn":null,
>      "validatedOn":"2021-01-05T13:00:00.000Z",
>      "categories":["cat_one/subcat_one","cat_one/subcat_two","cat_one/subcat_seven","cat_one/subcat_fifteen"],
>      "id":"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
>      "attempts":3,
>      "createdOn":"2021-01-01T00:00:00.000Z",
>      "relations":{},
>      "paymentTransactionId":123456,
>      "reviewer":null,
>      "canEditResponse":false,
>      "position":0,
>      "assetType":["web"],
>      "durationInSecs":86400,
>      "returnedForEditOn":null,
>      "status":"APPROVED",
>      "reviewerUser":null,
>      "claims":{"current":0,"limit":0},
>      "completedOn":"2021-01-01T20:00:00.000Z",
>      "deletedOn":null,
>      "description":"Does the application do this vulnerable thing? See https://www.google.com for more information about this test.",
>      "version":1,
>      "unauthorizedAssignees":null,
>      "hasBeenViewed":true,
>      "reviewedOn":"2021-01-01T00:00:00.000Z",
>      "structuredResponse":"no",
>      "campaignTemplate":{
>        "id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>        "version":1
>      },
>      "organization":{
>        "id":"AAAAAAAAAA",
>        "title":"Tom's Bakery"
>      },
>      "batch_id":"x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0",
>      "modifiedOn":"2020-10-27T01:23:43.013Z",
>      "scope":"",
>      "assigneeUser":{
>        "id":"srtmember1",
>        "email":"joe@shmoe.io"
>      },
>      "pausedDurationInSecs":0,
>      "responseType":"YES_NO",
>      "outcome":"PASS"
>    }
> ]
> ```

