# Terminology

This page is an alphabetical list of the terminology used within the Synack API

> Access Token
>> An Access Token is a unique identifier that is used to authenticate your browser session with the Synack Platform.
>> You can find it in your browser's Session Storage under the key `access_token`.
>>
>> In all examples of API usage within this document, it is expected that you will store you access token in a bash variables named `$accessToken` before running the curl command.
>>
>> For example:
>> ```bash
>> accessToken='abcabcabcabc..abc'
>> ```

> Campaign
>> A Campaign is a specific release of a target.
>> For example, if `SLEEPYPUPPY` is going to be available from SEP2020 to DEC2020, a campaign would be created and associated it.

> Listing
>> A Listing is an asset of an Organization that is to be tested.
>> For example, `Tom's Bakery`'s `Primary Website`, `Primary Network`, or `Primary Mobile App`

> Organization
>> An Organization is a company that has agreed to be tested by Synack.
>> For example, the ficticious `Tom's Bakery`

> Slug
>> A Slug is a short (10 character), unique, alphanumeric identifier for a specific organization or listing.
>> A Slug is used in the same way as a UUID, but sometimes one is used, and sometimes another is.

> Task
>> A task is most commonly known as a `Mission` outside of the context of the API.

> UUID
>> A UUID is a unique alphanumeric identifier for a specific organization or listing.
>> A UUID is used in the same way as a Slug, but sometimes one is used, and sometimes another is.
>> The pattern is <8 chars>-<4 chars>-<4 chars>-<4 chars>-<12 chars>.
>> This is a common data construct and you can find more information [here](https://en.wikipedia.org/wiki/Universally_unique_identifier).

