# Contributions

If you find an endpoint that isn't as well documented as you would like it to be, I highly encourage you to submit a PR with additional information.

When submitting additions or changes, please keep the following in mind.

## Format

Pages detailing an API will be broken up into several sections.
I haven't nailed down exactly what I want them to look like, but the [Tasks](../api/tasks/request_list.md) endpoint will likely be the first one I'll finish, so it should be a good reference of what I want them to look like.

## Data

All data within this document is ficticious.
DO NOT USE REAL DATA!!!

Synack takes the confidentiality of its clients very seriously and as a result, so do I.
There is no reason why the following organizations, targets, etc. should not be sufficient for the purpose of documenting the API.
If there is something missing that you think would be beneficial, please reach out to me directly.
Otherwise, please use the following data when making changes:

* Organization: `Tom's Bakery`
  * Slug: `AAAAAAAAAA`
  * UUID: `AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA`
  * Listings:
    * `Primary Website`
      * Codename: `SLEEPYSNAIL`
      * Type: `Web`
      * Slug: `0000000000`
      * UUID: `00000000-0000-0000-0000-000000000000`
      * Example Mission:
        * UUID: `aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa`
        * Campaign UUID: `zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz`
    * `Primary Network`
      * Codename: `SLEEPYPUPPY`
      * Type: `Host`
      * Slug: `1111111111`
      * UUID: `11111111-1111-1111-1111-111111111111`
      * Example Mission:
        * UUID: `bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb`
        * Campaign UUID: `yyyyyyyy-yyyy-yyyy-yyyy-yyyyyyyyyyyy`
    * `Primary Mobile App`
      * Codename: `SLEEPYKITTEN`
      * Type: `Mobile`
      * Slug: `2222222222`
      * UUID: `22222222-2222-2222-2222-222222222222`
      * Example Mission:
        * UUID: `cccccccc-cccc-cccc-cccc-cccccccccccc`
        * Campaign UUID: `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`

* Synack Red Team Member: `Joe Shmoe`
  * User ID (slug): `srtmember1`
  * Email: `joe@shmoe.io`

If something like a UUID is returned in the data and it isn't necessarily something that anyone is likely to specifically care about, use one of the following:
* Generic UUID: `x0x0x0x0-x0x0-x0x0-x0x0-x0x0x0x0x0x0`
