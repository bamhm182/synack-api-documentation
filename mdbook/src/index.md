# Introduction

Welcome to the **Unofficial** Synack API Documentation!

This is a living document that I have put together to document the Synack API.
It is mostly a personal reference, but I hope that others are able to use it to build better tools for themselves.
If you find anything that is missing, I encourage you to submit a PR.

PLEASE keep the following in mind if you use anything in this document.

1. While the Synack API is usable by external tools, it wasn't designed for it and support for its use is limited.
1. DO NOT make more requests than necessary.
It may be tempting to blow up the API to try and get the most up to date information possible. 
If you are polling endpoints constantly, you are doing it wrong.
Think outside the box to determine a way to get the information you're after as the result of something else happening or poll very infrequently.
[You will be kicked off the platform for blowing up the API](https://support.synack.com/hc/en-us/articles/1500002201401-Mission-Automation-Throttling-MUST-READ).
1. I have explicitly created fake organizations, targets, and tasks for the purpose of this API.
If you want to contribute, you must check out the [Contributions](./contributions/index.md) page for information about the ones I have created.
Under NO circumstances should you think about trying to add any real names, codenames, UUIDs, slugs, or any other identifying data from any part of the actual Synack API.

Keep in mind that I am not to be held responsible if you use or misuse the information in this document and as a result are kicked off Synack or suffer any other negative repercussions.

I recommend you check out the [Contributions](./contributions/index.md) page even if you don't intend to contribute as I have specfied the data I will be using as examples there and certain things may make more sense if you've read it already.
